#!/usr/bin/python
# =============================================================================
# File  : functions.py (Spring 2011)
# Author: Troy Stump (troystump@csu.fullerton.edu)
# CWID  : 894788678
# Class : 386 Professor Shafae
# Assign: 06 (FINAL PROJECT)
# =============================================================================
# This program is a list of my commonly used functions throughout other classes
# relating to the game: CIRCUS ASSAULT.
# =============================================================================

import math
import os
import fnmatch
import pygame
from pygame.constants import RLEACCEL


# === LoadGraphic =============================================================
# This method loads an image into memory, converts it, and can set the image's
# pixel transparency based on the colorkey value. 
#
#    input:
#            fullPath   --  path to the image, relative to the calling object
#            colorkey   --  -1 : (x=0, y=0) color transparency
#                           -2 : retain alpha transparency
#
#    output:
#            returns a tuple of the image an it's corresponding rect
# =============================================================================

def LoadGraphic(fullPath, colorkey = None):
    try:
        image = pygame.image.load(fullPath)
    except pygame.error, message:
        print 'Cannot load graphic:', fullPath
        raise SystemExit, message

    # -2 colorkey value preserves alpha transparency images
    if (colorkey == -2):
        image = image.convert_alpha()
        colorkey = None
    else:
        image = image.convert()

    if colorkey is not None:
        if colorkey is -1:
            colorkey = image.get_at( (0,0) )
        
        image.set_colorkey(colorkey, RLEACCEL)

    return image, image.get_rect()


# === LoadSound ===============================================================
# This method will load a sound object into memory 
#
#    input:
#            fullPath   --  path to the sound, relative to the calling object
#
#    output:
#            returns the sound as an object
# =============================================================================

def LoadSound(fullPath):
    class NoneSound:
        def play(self):
            pass

    if not pygame.mixer:
        return NoneSound()

    try:
        sound = pygame.mixer.Sound(fullPath)
    except pygame.error, message:
        print 'Cannot load sound:', fullPath
        raise SystemExit, message

    return sound


# === RotateImage =============================================================
# This method will rotate an image based on a position by using a corresponding
# rect object.
#
#    input:
#            image      --  image to rotate
#            position   --  position to face in the rotation
#            rect       --  holds the coordinates that are the focal point of
#                           rotation.
#
#    output:
#            returns the rotated image
# =============================================================================

def RotateImage(image, rect, position):
    x, y = position
    rotateX = rect.centerx - x
    rotateY = rect.centery - y
    angle = 180 - math.atan2(rotateY, rotateX) * (180 / math.pi)

    return pygame.transform.rotate(image, angle)


# === ReplaceAll ==============================================================
# This method will replace all text in a string of text by referring to a
# replacement dictionary.
#
#    input:
#            text         --  string of text
#            dictionary   --  dictionary holding replacement information
#
#    output:
#            returns the rotated image
# =============================================================================

def ReplaceAll(text, dictionary):
    for word, replacement in dictionary.iteritems():
        text = text.replace(word, replacement)
    return text


# === FindPathSeparator =======================================================
# This method will retrieve the path separator used by the user's file system.
#
#    input:
#            nothing
#
#    output:
#            returns the correct path separator
# =============================================================================

def FindPathSeparator():
    presentWorkingDirectory = os.getcwd()
    currentSeparator = "/"
    neededSeparator = "\\"

    if (fnmatch.fnmatch(presentWorkingDirectory, neededSeparator)):
        currentSeparator, neededSeparator = neededSeparator, currentSeparator
        
    return str(currentSeparator)