#!/usr/bin/python
# =============================================================================
# File  : config.py (Spring 2011)
# Author: Troy Stump (troystump@csu.fullerton.edu)
# CWID  : 894788678
# Class : 386 Professor Shafae
# Assign: 06 (FINAL PROJECT)
# =============================================================================
# This program is the main configuration file for the game: CIRCUS ASSAULT.
# =============================================================================

import pygame
from .. import functions #@UnresolvedImport
import os


# === Center Display Window ===================================================
os.environ["SDL_VIDEO_CENTERED"] = "1"



# === Configure Pygame Modules ================================================
pygame.mixer.pre_init(22050, -16, 2, 1024)
pygame.mixer.init()

pygame.init()

screen_width = 640
screen_height = 480
screen = pygame.display.set_mode( (screen_width, screen_height))

pygame.mouse.set_cursor(*pygame.cursors.broken_x)



# === Mouse Buttons Indices ===================================================
LEFT = 1
MIDDLE = 2
RIGHT = 3



# === Path Indices ============================================================
MUSIC_PATH = 0
SOUNDS_PATH = 1
GRAPHICS_PATH = 2
FONTS_PATH = 3



# === File Extensions Used ====================================================
MUSIC_EXTENSION = ".ogg"
SOUND_EXTENSION = ".ogg"
GRAPHIC_EXTENSION = ".png" 
FONT_EXTENSION = ".otf"



# === File System's Path Separator ============================================
path_separator = functions.FindPathSeparator()

# === Relative File Paths =====================================================
paths = ["data" + path_separator + "audio" + path_separator + "music" + path_separator,
         "data" + path_separator + "audio" + path_separator + "sounds" + path_separator,
         "data" + path_separator + "graphics" + path_separator,
         "data" + path_separator + "fonts" + path_separator]



# === MUSIC FILES =============================================================
music_game_background = paths[MUSIC_PATH] + "music_game_background" + MUSIC_EXTENSION
music_game_title = paths[MUSIC_PATH] + "music_game_title" + MUSIC_EXTENSION



# === SOUND FILES =============================================================
sound_game_gameOver = functions.LoadSound(paths[SOUNDS_PATH] + "sound_game_gameOver" + SOUND_EXTENSION)
sound_gun_fire = functions.LoadSound(paths[SOUNDS_PATH] + "sound_gun_fire" + SOUND_EXTENSION)
sound_gun_outOfAmmo = functions.LoadSound(paths[SOUNDS_PATH] + "sound_gun_outOfAmmo" + SOUND_EXTENSION)
sound_gun_reload = functions.LoadSound(paths[SOUNDS_PATH] + "sound_gun_reload" + SOUND_EXTENSION)
sound_level_waveComplete = functions.LoadSound(paths[SOUNDS_PATH] + "sound_level_waveComplete" + SOUND_EXTENSION)
sound_player_killEnemy = functions.LoadSound(paths[SOUNDS_PATH] + "sound_player_killEnemy" + SOUND_EXTENSION)
sound_player_takeDamage = functions.LoadSound(paths[SOUNDS_PATH] + "sound_player_takeDamage" + SOUND_EXTENSION)

sound_gun_fire.set_volume(.15)
sound_gun_outOfAmmo.set_volume(.15)
sound_gun_reload.set_volume(.15)
sound_level_waveComplete.set_volume(.3)
sound_player_killEnemy.set_volume(1)
sound_player_takeDamage.set_volume(.3)


# === GRAPHIC FILES ===========================================================
graphic_bullet_bullet, rect_bullet_bullet = functions.LoadGraphic(paths[GRAPHICS_PATH] + "graphic_bullet_bullet" + GRAPHIC_EXTENSION, -1)
graphic_enemy_zombie_01, rect_enemy_zombie_01 = functions.LoadGraphic(paths[GRAPHICS_PATH] + "graphic_enemy_zombie_01" + GRAPHIC_EXTENSION, -1)
graphic_enemy_zombie_02, rect_enemy_zombie_02 = functions.LoadGraphic(paths[GRAPHICS_PATH] + "graphic_enemy_zombie_02" + GRAPHIC_EXTENSION, -1)
graphic_enemy_zombie_03, rect_enemy_zombie_03 = functions.LoadGraphic(paths[GRAPHICS_PATH] + "graphic_enemy_zombie_03" + GRAPHIC_EXTENSION, -1)
graphic_enemy_zombie_takeDamage, rect_enemy_zombie_takeDamage = functions.LoadGraphic(paths[GRAPHICS_PATH] + "graphic_enemy_zombie_takeDamage" + GRAPHIC_EXTENSION, -1)
graphic_game_background, rect_game_background = functions.LoadGraphic(paths[GRAPHICS_PATH] + "graphic_game_background" + GRAPHIC_EXTENSION, -1)
graphic_game_gameover, rect_game_gameover = functions.LoadGraphic(paths[GRAPHICS_PATH] + "graphic_game_gameover" + GRAPHIC_EXTENSION, -1)
graphic_game_titleScreenBg, rect_game_titleScreenBg = functions.LoadGraphic(paths[GRAPHICS_PATH] + "graphic_game_titleScreenBg" + GRAPHIC_EXTENSION, -1)
graphic_game_titleScreenPlayGame, rect_game_titleScreenPlayGame = functions.LoadGraphic(paths[GRAPHICS_PATH] + "graphic_game_titleScreenPlayGame" + GRAPHIC_EXTENSION, -2)
graphic_game_titleScreenPlayGameMouseOver, rect_game_titleScreenPlayGameMouseOver = functions.LoadGraphic(paths[GRAPHICS_PATH] + "graphic_game_titleScreenPlayGameMouseOver" + GRAPHIC_EXTENSION, -2)
graphic_game_titleScreenPlayGameMouseOverArrows, rect_game_titleScreenPlayGameMouseOverArrows = functions.LoadGraphic(paths[GRAPHICS_PATH] + "graphic_game_titleScreenPlayGameMouseOverArrows" + GRAPHIC_EXTENSION, -2)
graphic_game_tutorial, rect_game_tutorial = functions.LoadGraphic(paths[GRAPHICS_PATH] + "graphic_game_tutorial" + GRAPHIC_EXTENSION, -1)
graphic_gun_fire, rect_gun_fire = functions.LoadGraphic(paths[GRAPHICS_PATH] + "graphic_gun_fire" + GRAPHIC_EXTENSION, -1)
graphic_gun_gun, rect_gun_gun = functions.LoadGraphic(paths[GRAPHICS_PATH] + "graphic_gun_gun" + GRAPHIC_EXTENSION, -1)
graphic_icon_exe, rect_icon_exe = functions.LoadGraphic(paths[GRAPHICS_PATH] + "graphic_icon_exe" + GRAPHIC_EXTENSION, -2)
graphic_player_player, rect_player_player = functions.LoadGraphic(paths[GRAPHICS_PATH] + "graphic_player_player" + GRAPHIC_EXTENSION, -1)
graphic_player_player_takeDamage, rect_player_player_takeDamage = functions.LoadGraphic(paths[GRAPHICS_PATH] + "graphic_player_player_takeDamage" + GRAPHIC_EXTENSION, -1)

# === Master Zombie List ======================================================
list_graphics_enemies_zombies = [graphic_enemy_zombie_01, graphic_enemy_zombie_02, graphic_enemy_zombie_03, graphic_enemy_zombie_takeDamage]

# === Set Application Icon ==================================================
pygame.display.set_icon(graphic_icon_exe)



# === FONT FILES ==============================================================
font_hobo = paths[FONTS_PATH] + "font_hobo" + FONT_EXTENSION

top_bar_font = pygame.font.Font(font_hobo, 14)
level_wave_complete_font = pygame.font.Font(font_hobo, 60)