#!/usr/bin/python
# =============================================================================
# File  : CBullet.py (Spring 2011)
# Author: Troy Stump (troystump@csu.fullerton.edu)
# CWID  : 894788678
# Class : 386 Professor Shafae
# Assign: 06 (FINAL PROJECT)
# =============================================================================
# This class simulates a bullet.
# =============================================================================

import pygame
from math import sqrt


class CBullet(pygame.sprite.Sprite):
    # === __init__ ============================================================
    # This is the class initializer method.
    #
    #    input:
    #            screenX        --  width of display screen
    #            screenY        --  height of display screen
    #            bullet_image   --  image of bullet
    #            bullet_rect    --  rect of bullet
    #
    #    output:
    #            nothing
    # =========================================================================

    def __init__(self, screenX, screenY, bullet_image, bullet_rect):  
        pygame.sprite.Sprite.__init__(self)
        self.defaultImage, self.defaultRect = bullet_image, bullet_rect
        self.defaultRect.center = (screenX / 2, screenY - 35 - (self.defaultRect.height / 2)) 
        self.clearImage = self.defaultImage.copy()
        self.clearImage.fill( (255, 255, 255))
        self.speed = 36
        self.velocityX = None
        self.velocityY = None
        self.isMoving = False

        self.image = self.clearImage.copy()
        self.rect = self.defaultRect.copy()


    # === update ==============================================================
    # This method updates all components within the bullet class, making sure
    # the bullet is killed if leaving the display screen and also having the
    # bullet move in the direction given from the posiion of the mouse 
    #
    #    input:
    #            mousePosition   -- (x,y) coordinates of the mouse location
    #
    #    output:
    #            nothing
    # =========================================================================

    def update(self, mousePosition):
        if (self.isMoving == False):
            return

        # if half the bullet goes outside the display window, kill it
        if ( (self.rect.centerx < 0)  or  (self.rect.centerx > 640)  or  (self.rect.centery < 0)  or  (self.rect.centery > 480) ):
            # TODO: add explosion animation
            self.isMoving = False
            self.velocityX = None
            self.velocityY = None
            self.kill()

            self.image, self.rect = self.clearImage.copy(), self.defaultRect.copy()

        elif ( (self.isMoving == True)  and  (self.velocityX == self.velocityY == None) ):
            self.image = self.defaultImage.copy()
            x, y = mousePosition
            positionX = x - self.rect.centerx
            positionY = y - self.rect.centery
            hypotenuse = sqrt( (positionX * positionX) + (positionY * positionY) )
            
            if (hypotenuse == 0):
                hypotenuse = 0.1

            self.velocityX = (positionX / hypotenuse) * self.speed
            self.velocityY = (positionY / hypotenuse) * self.speed
            self.rect.centerx += self.velocityX * 2
            self.rect.centery += self.velocityY * 2

        elif (self.isMoving == True):
            # here we lose the precision of our velocity components since we
            # have not implemented a rasterization algorithm. In an attempt to
            # stay as accurate as possible in the bullets trajectory, we will
            # try and increase the velocity so less calculations are performed
            # before the bullet dies. This should alleviate some of the
            # precision lost
            self.rect.centerx += self.velocityX
            self.rect.centery += self.velocityY