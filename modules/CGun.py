#!/usr/bin/python
# =============================================================================
# File  : CGun.py (Spring 2011)
# Author: Troy Stump (troystump@csu.fullerton.edu)
# CWID  : 894788678
# Class : 386 Professor Shafae
# Assign: 06 (FINAL PROJECT)
# =============================================================================
# This class simulates a gun.
# =============================================================================

import pygame
from functions import RotateImage


class CGun(pygame.sprite.Sprite):
    # === __init__ ============================================================
    # This is the class initializer method.
    #
    #    input:
    #            screenX          --  width of display screen
    #            screenY          --  height of display screen
    #            gun_image        --  image of the gun
    #            gun_rect         --  rect of the fun
    #            gun_fire_image   --  image of the gun's firing burst
    #            gun_fire_rect    --  rect of the gun's firing burst
    #
    #    output:
    #            nothing
    # =========================================================================

    def __init__(self, screenX, screenY, gun_image, gun_rect, gun_fire_image, gun_fire_rect):  
        pygame.sprite.Sprite.__init__(self)
        self.image, self.rect = gun_fire_image, gun_fire_rect
        self.rect.center = (screenX / 2, screenY - 10 - (self.rect.height / 2))
        self.currentGunImage, self.currentGunRect = gun_image, gun_rect
        
        self.isGunBurst = False


    # === update ==============================================================
    # This method updates all components of the gun, including the rotation of
    # the gun firing burst to match the mouse location.
    #
    #    input:
    #            mousePosition   -- (x,y) coordinates of the mouse location
    #
    #    output:
    #            nothing
    # =========================================================================

    def update(self, mousePosition):
        self.rotateImage = RotateImage(self.image, self.rect, mousePosition)
        self.rotateRect = self.rotateImage.get_rect()
        self.rotateRect.center = self.rect.center

        if (self.isGunBurst == False):
            self.rotateImage.fill( (255, 255, 255) )


    # === Draw ================================================================
    # This method will draw the gun's firing burst to the screen, reflecting
    # the rotation given in the update function. 
    #
    #    input:
    #            surface   -- the surface to draw the gun firing burst on
    #
    #    output:
    #            nothing
    # =========================================================================

    def Draw(self, surface):
        surface.blit(self.rotateImage, self.rotateRect)