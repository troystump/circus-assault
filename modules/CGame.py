#!/usr/bin/python
# =============================================================================
# File  : CGame.py (Spring 2011)
# Author: Troy Stump (troystump@csu.fullerton.edu)
# CWID  : 894788678
# Class : 386 Professor Shafae
# Assign: 06 (FINAL PROJECT)
# =============================================================================
# This program is the driver for the game CIRCUS ASSAULT.
# =============================================================================


from config.config import *
from math import ceil
from CPlayer import CPlayer
from CEnemy import CEnemy

class CGame:
    # === init ================================================================
    # This is the class initializer method.
    # 
    # input:
    #        nothing
    #
    # output
    #        nothing
    # =========================================================================

    def __init__(self):
        # fullscreen toggle flag
        self.fullscreen = False
        
        # game states
        self.gameState = ["Title Screen"]
        self.finished = False

        # player
        self.player = CPlayer(screen_width, screen_height,
                              graphic_player_player, graphic_player_player_takeDamage, rect_player_player,
                              graphic_gun_gun, rect_gun_gun,
                              graphic_gun_fire, rect_gun_fire,
                              graphic_bullet_bullet, rect_bullet_bullet,
                              sound_gun_fire, sound_gun_outOfAmmo, sound_gun_reload)
        # enemy
        self.enemies = [CEnemy(screen_width, 0, list_graphics_enemies_zombies) for i in xrange(0, 2)]

        # sprite groups
        self.playerGroup = pygame.sprite.RenderUpdates()
        self.playerGroup.add(self.player)

        self.enemyGroup = pygame.sprite.RenderUpdates()
        self.enemyGroup.add(self.enemies)

        # counters
        self.FPS = 60
        self.clock = pygame.time.Clock()
        


    # === Run =================================================================
    # This method holds the states of the game and pops each state off a stack.
    # The stack is popped upon each re-entry to this method. If there are no
    # more game states on the stack, the game is over.
    # 
    # input:
    #        nothing
    #
    # output
    #        nothing
    # =========================================================================

    def Run(self):
        while (True):
            
            if (not self.gameState):
                break
            
            nextState = self.gameState.pop()
            
            print "Returned to main loop. Now switching to: " + nextState
            
            functionName = nextState.replace(" " , "")
            
            if hasattr(self, functionName):
                function = getattr(self, functionName)
                function()
            else:
                break

        print "Game over. Thanks for playing!"
        
        
        
    # === TitleScreen =========================================================
    # This method handles the title screen game state. Currently, this screen
    # only allows the user to start the game, but an options menu will be added
    # that allows the changing of the music volume, etc.
    # 
    # input:
    #        nothing
    #
    # output
    #        nothing
    # =========================================================================

    def TitleScreen(self):
        # background music to be played
        pygame.mixer.music.load(music_game_title)
        pygame.mixer.music.set_volume(.6)
        
        playGame = None
        playGameRect = rect_game_titleScreenPlayGame.copy()
        playGameRect.center = (screen_width / 2, screen_height / 2)
        
        playGameMouseOffArrows = graphic_game_titleScreenPlayGameMouseOverArrows.copy()
        playGameMouseOffArrows.fill((0, 0, 0, 0))
        
        playGameMouseOverStatus = None
        playGameMouseOverStatusRect = rect_game_titleScreenPlayGameMouseOverArrows.copy()
        playGameMouseOverStatusRect.center = (screen_width / 2, screen_height / 2)
        
        playerClick = False

        while (not self.finished):
            self.clock.tick(self.FPS)
            
            for event in pygame.event.get():
                # if the user presses escape or closes the window, exit the game
                if ( (event.type == pygame.QUIT)  or  (event.type == pygame.KEYDOWN  and  event.key == pygame.K_ESCAPE) ):
                    pygame.quit()
                    return

                elif (event.type == pygame.KEYDOWN  and  event.key == pygame.K_f):
                    self.fullscreen = not self.fullscreen

                    if (self.fullscreen == True):
                        pygame.display.set_mode( (screen_width, screen_height), pygame.FULLSCREEN )
                    else:
                        pygame.display.set_mode( (screen_width, screen_height) )

                elif ( (event.type == pygame.MOUSEBUTTONDOWN)  and  (event.button == LEFT) ):
                    playerClick = True
            
            # if player clicks on the "PLAY GAME" text, start up the next state
            if (playGameRect.collidepoint(pygame.mouse.get_pos())):
                playGameMouseOverStatus = graphic_game_titleScreenPlayGameMouseOverArrows
                playGame = graphic_game_titleScreenPlayGameMouseOver

                if (playerClick == True):
                    sound_player_killEnemy.play()
                    self.gameState.append("How To Play")
                    self.finished = True
                    pygame.time.wait(1000)
            else:
                playGameMouseOverStatus = playGameMouseOffArrows
                playGame = graphic_game_titleScreenPlayGame

            # restart the title screen music when it stops
            if (pygame.mixer.music.get_busy() == False):
                pygame.mixer.music.play()
            
            pygame.display.set_caption("CIRCUS ASSAULT (%d FPS)" % self.clock.get_fps()) 
            
            screen.fill( (0, 0, 0) )
            screen.blit(graphic_game_titleScreenBg, (0, 0))
            screen.blit(playGame, playGameRect)
            screen.blit(playGameMouseOverStatus, playGameMouseOverStatusRect)
            pygame.display.update()



    # === HowToPlay ===========================================================
    # This method handles the tutorial overview of how to play the game.
    # 
    # input:
    #        nothing
    #
    # output
    #        nothing
    # =========================================================================

    def HowToPlay(self):
        self.finished = not self.finished

        pygame.event.clear()

        while (not self.finished):
            self.clock.tick(self.FPS)

            for event in pygame.event.get():
                # if the user presses escape or closes the window, exit the game
                if ( (event.type == pygame.QUIT)  or  (event.type == pygame.KEYDOWN  and  event.key == pygame.K_ESCAPE) ):
                    pygame.quit()
                    return
                
                elif (event.type == pygame.KEYDOWN  and  event.key == pygame.K_f):
                    self.fullscreen = not self.fullscreen

                    if (self.fullscreen == True):
                        pygame.display.set_mode( (screen_width, screen_height), pygame.FULLSCREEN )
                    else:
                        pygame.display.set_mode( (screen_width, screen_height) )

                elif (event.type == pygame.KEYDOWN  and event.key == pygame.K_SPACE):
                    self.gameState.append("Play Game")
                    self.finished = True
                    pygame.time.wait(1000)

            if (pygame.mixer.music.get_busy() == False):
                pygame.mixer.music.play()

            pygame.display.set_caption("CIRCUS ASSAULT (%d FPS)" % self.clock.get_fps())  
        
            screen.fill( (0, 0, 0) )
            screen.blit(graphic_game_tutorial, (0, 0))
            pygame.display.update()



    # === FadeLevelIn =========================================================
    # This method fades in a level by first blitting the incoming surface as
    # the level's background. Next, a circle is then drawn on top of the
    # background surface. At first, this circle is filled up to a radius
    # equal to half the longest side of the display window. This continues and
    # each subsequent circle drawn fills to a smaller and smaller width, until
    # the width falls below a value of 1. 
    # 
    # input:
    #        surface   --  background surface to draw to the screen
    #
    # output
    #        nothing
    # =========================================================================

    def FadeLevelIn(self, surface):
        pygame.mixer.music.stop()

        DRAW_CIRCLE = pygame.USEREVENT
        pygame.time.set_timer(DRAW_CIRCLE, 1)

        finishedDrawing = False
        
        drawSpeed = 4
        
        if (screen_width > screen_height):
            maxRadius = screen_width
        else:
            maxRadius = screen_height
            
        radius = width = maxRadius

        while (not finishedDrawing):
            self.clock.tick(self.FPS)
            
            for event in pygame.event.get():
                # if the user presses escape or closes the window, exit the game
                if ( (event.type == pygame.QUIT)  or  (event.type == pygame.KEYDOWN  and  event.key == pygame.K_ESCAPE) ):
                    pygame.quit()
                    return

                elif (event.type == pygame.KEYDOWN  and  event.key == pygame.K_f):
                    self.fullscreen = not self.fullscreen

                    if (self.fullscreen == True):
                        pygame.display.set_mode( (screen_width, screen_height), pygame.FULLSCREEN )
                    else:
                        pygame.display.set_mode( (screen_width, screen_height) )
                
                elif (event.type == DRAW_CIRCLE):
                    width -= drawSpeed

            if (width < 1):
                return

            pygame.display.set_caption("CIRCUS ASSAULT (%d FPS)" % self.clock.get_fps())
        
            screen.fill( (0, 0, 0) )
            screen.blit(surface, (0, 0))
            pygame.draw.circle(screen, (0, 0, 0, 255), (screen_width / 2, screen_height / 2), radius, width)
            pygame.display.update()



    # === PlayGame ============================================================
    # This method handles the play game state. This is the method where the
    # actual game is played by the player.
    # 
    # input:
    #        nothing
    #
    # output
    #        nothing
    # =========================================================================

    def PlayGame(self):
        self.FadeLevelIn(graphic_game_background)
        self.finished = not self.finished

        enemiesPerWave = 2
        self.player.currentWave = 1
        enemiesKilled = 0;
        
        topBarStats = None
        topBarStats2 = None
        top_bar_font.set_bold(False)

        levelComplete = level_wave_complete_font.render("WAVE COMPLETE!", 1, (255, 0, 0))
        levelComplete.set_colorkey( (255, 255, 255, 0), pygame.RLEACCEL )
        
        levelStatus = levelComplete.copy()
        levelStatusRect = levelStatus.get_rect()
        levelStatus.fill( (255, 255, 255, 0) )

    
        topBar = pygame.Surface( (screen_width, top_bar_font.get_height() * 2) ).convert()
        topBar.set_alpha(30, pygame.RLEACCEL)
        topBar.fill( (0, 200, 0) )
        
        # setup the background music to be played
        pygame.mixer.music.load(music_game_background)
        pygame.mixer.music.set_volume(.6)
    
    
        # flags
        isWaveComplete = False
        soundLength = 0
        ticks = 0
    
        INCREMENT_TIMER = pygame.USEREVENT
        pygame.time.set_timer(INCREMENT_TIMER, 1000)
    
        while (not self.finished):
            self.clock.tick(self.FPS)
    
            # event handler
            for event in pygame.event.get():
                # if the user presses escape or closes the window, exit the game
                if ( (event.type == pygame.QUIT)  or  (event.type == pygame.KEYDOWN  and  event.key == pygame.K_ESCAPE) ):
                    pygame.quit()
                    return
                
                elif (event.type == INCREMENT_TIMER):
                    self.player.secondsAlive += 1
                    
                elif (event.type == pygame.KEYDOWN  and  event.key == pygame.K_f):
                    self.fullscreen = not self.fullscreen

                    if (self.fullscreen == True):
                        pygame.display.set_mode( (screen_width, screen_height), pygame.FULLSCREEN )
                    else:
                        pygame.display.set_mode( (screen_width, screen_height) )
                
                # if the user presses R on the keyboard, have the player attempt to
                # reload the gun's magazine
                elif ( (event.type == pygame.KEYDOWN)  and  (event.key == pygame.K_r) )  or  ( (event.type == pygame.MOUSEBUTTONDOWN)  and  (event.button == RIGHT) ):
                    self.player.Reload()
                
                elif (isWaveComplete == True):
                    break
    
                # if the user clicks the left mouse button, have the player attempt
                # to fire a bullet
                elif ( (event.type == pygame.MOUSEBUTTONDOWN)  and  (event.button == LEFT) ):
                    self.player.Fire()
            
            # update everything
            mousePosition = pygame.mouse.get_pos()
            self.playerGroup.update(mousePosition)
            self.enemyGroup.update(self.player.rect.center)

            if (pygame.mixer.music.get_busy() == False):
                pygame.mixer.music.play()

            if ( (isWaveComplete == True)  and  pygame.time.get_ticks() - ticks  >= soundLength):
                levelStatus.fill( (255, 255, 255, 0) )
                self.player.currentWave += 1
                enemiesPerWave += 2
                self.enemies = [CEnemy(screen_width, 0, list_graphics_enemies_zombies) for i in xrange(enemiesPerWave)]
                self.enemyGroup.add(self.enemies)
                isWaveComplete = False
                enemiesKilled = 0
    
            # if the player is confronted with any zombie, then the player will
            # take damage
            enemy = pygame.sprite.spritecollideany(self.player, self.enemyGroup)
            if (enemy != None):
                sound_player_takeDamage.stop()
                sound_player_takeDamage.play()
                self.player.hitPoints -= enemy.damage
                self.player.rotateImage = self.player.rotateHurtImage
                enemy.Reset()
                enemiesKilled += 1
                enemy.kill()
                if (self.player.hitPoints == 0):
                    self.player.kill()
                    sound_player_takeDamage.stop()
                    pygame.mixer.music.stop()
                    sound_game_gameOver.play()
                    self.gameState.append("Game Over")
                    self.finished = True
                    pygame.time.wait(1000)
                    continue

            for hit in pygame.sprite.groupcollide(self.enemyGroup, self.player.bulletGroup, 0, 0):
                if (hit.isDamageTaken == True):
                    continue

                hit.hitPoints -= 1

                if (hit.hitPoints == 0):
                    hit.kill()
                    sound_player_killEnemy.stop()
                    sound_player_killEnemy.play()
                    hit.hitPoints = hit.maxHitPoints
                    hit.image, hit.rect = hit.defaultImage.copy(), hit.defaultRect.copy()
                    self.player.score += hit.pointValue
                    enemiesKilled += 1
                else:
                    hit.isDamageTaken = True
                
            if ( (enemiesKilled == enemiesPerWave)  and  (isWaveComplete == False) ):
                sound_player_killEnemy.stop()
                sound_level_waveComplete.stop()
                sound_level_waveComplete.play()
                soundLength = int( ceil(sound_level_waveComplete.get_length()) ) * 1000
                ticks = pygame.time.get_ticks()
                levelStatus.blit(levelComplete, (0, 0))
                isWaveComplete = True
                self.enemies = [CEnemy(screen_width, 0, list_graphics_enemies_zombies) for i in xrange(enemiesPerWave)]

            pygame.display.set_caption("CIRCUS ASSAULT (%d FPS)" % self.clock.get_fps())

            topBarStats = top_bar_font.render("AMMO: %d / %d                         WAVE: %d       ENEMIES: %d / %d       SCORE: %d" % (self.player.bulletsRemaining, self.player.maxBullets, self.player.currentWave, len(self.enemyGroup), enemiesPerWave, self.player.score,), 1, (255, 0, 0))
            topBarStats2 = top_bar_font.render("[ PRESS 'R' TO RELOAD ]       HEALTH: %d / %d       TIME ALIVE: %d" % ( self.player.hitPoints, self.player.maxHitPoints, self.player.secondsAlive), 1, (255, 0, 0))

    
            # draw everything
            screen.fill( (0, 0, 0) )
            screen.blit(graphic_game_background, (0, 0))
            screen.blit(topBar, (0, 0))
            screen.blit(self.player.gun.currentGunImage, (0, 0))
            self.player.Draw(screen)
            self.enemyGroup.draw(screen)
            screen.blit(topBarStats, (rect_gun_gun.width + 10, 0))
            screen.blit(topBarStats2, (rect_gun_gun.width + 10, topBarStats.get_height()))
            screen.blit(levelStatus, ( (screen_width - levelStatusRect.width) / 2, (screen_height - levelStatusRect.height) / 2) )
            pygame.display.update()


    # === GameOver ============================================================
    # This method handles the play over state. Nothing much to this, just
    # output a gameover screen until the user adds anything to the event queue.
    # 
    # input:
    #        nothing
    #
    # output
    #        nothing
    # =========================================================================

    def GameOver(self):        
        self.finished = not self.finished

        top_bar_font.set_bold(True)
        playerWaveCount = top_bar_font.render("Z O M B I E  W A V E :  %d" % (self.player.currentWave), 1, (0, 0, 0))
        playerTimeAlive = top_bar_font.render("T I M E  A L I V E :  %d  S E C O N D S" % (self.player.secondsAlive), 1, (0, 0, 0))

        pygame.mixer.music.stop()

        pygame.event.clear()

        while (not self.finished):
            self.clock.tick(self.FPS)

            for event in pygame.event.get():
                # if the user presses escape or closes the window, exit the game
                if ( (event.type == pygame.QUIT)  or  (event.type == pygame.KEYDOWN  and  event.key == pygame.K_ESCAPE) ):
                    pygame.quit()
                    return

                elif (event.type == pygame.KEYDOWN  and  event.key == pygame.K_f):
                    self.fullscreen = not self.fullscreen

                    if (self.fullscreen == True):
                        pygame.display.set_mode( (screen_width, screen_height), pygame.FULLSCREEN )
                    else:
                        pygame.display.set_mode( (screen_width, screen_height) )

                elif ( (event.type == pygame.KEYDOWN)  or  (event.type == pygame.MOUSEBUTTONDOWN) ):
                    self.gameState.append("__init__")
                    self.finished = True
                    pygame.time.wait(1000)
    
            pygame.display.set_caption("CIRCUS ASSAULT (%d FPS)" % self.clock.get_fps())  
        
            screen.fill( (0, 0, 0) )
            screen.blit(graphic_game_gameover, (0, 0))
            screen.blit(playerWaveCount, ( (screen_width - playerWaveCount.get_width()) / 2, ((screen_height - playerWaveCount.get_height()) / 2) + 80) )
            screen.blit(playerTimeAlive, ( (screen_width - playerTimeAlive.get_width()) / 2, ((screen_height - playerTimeAlive.get_height()) / 2) + playerWaveCount.get_height() + 80) )
            pygame.display.update()