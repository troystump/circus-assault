#!/usr/bin/python
# =============================================================================
# File  : CEnemy.py (Spring 2011)
# Author: Troy Stump (troystump@csu.fullerton.edu)
# CWID  : 894788678
# Class : 386 Professor Shafae
# Assign: 06 (FINAL PROJECT)
# =============================================================================
# This class simulates an enemy zombie.
# =============================================================================

import pygame
from random import seed, randint, randrange
from math import sqrt

class CEnemy(pygame.sprite.Sprite):

    # === __init__ ============================================================
    # This is the class initializer method.
    #
    #    input:
    #            screenX                         --  width of display screen
    #            screenY                         --  height of display screen
    #            list_graphics_enemies_zombies   --  images of zombies
    #
    #    output:
    #            nothing
    # =========================================================================

    def __init__(self, screenX, screenY, list_graphics_enemies_zombies): 
        pygame.sprite.Sprite.__init__(self)

        self.screenX, self.screenY = screenX, screenY
        
        seed()

        # draw a random zombie image to use as enemy sprite
        zombieListSize = len(list_graphics_enemies_zombies)
        randomZombie = randint(0, zombieListSize - 2)
        
        # save the original image surface and rect
        self.defaultImage, self.defaultRect = list_graphics_enemies_zombies[randomZombie], list_graphics_enemies_zombies[randomZombie].get_rect() 
        self.defaultRect.center = ( randint(0, screenX), randint(screenY * -1, 10) )

        self.hurtImage = list_graphics_enemies_zombies[zombieListSize - 1]

        # these will be used for rotational manipulation of the sprite
        self.image, self.rect = self.defaultImage.copy(), self.defaultRect.copy()
        self.rect.inflate_ip(-1, -1)

        
        # current angle to rotate the sprite
        self.rotationAngle = 0

        self.rotation = 0
        
        # we will increment this 
        self.rotationCounter = 0
        
        self.hitPoints = self.maxHitPoints = 2
        self.pointValue = 15

        self.damage = 1
        self.speed = 2
        
        self.isDamageTaken = False
        self.damageTick = 0
        self.damageTickMax = 4


    # === update ==============================================================
    # This method updates all components of the enemy zombie, including
    # a pseudo random speed and direction of the zombie every 45 degrees of
    # rotation. 
    #
    #    input:
    #            playerPosition   --  (x,y) coordinates of the player's location
    #
    #    output:
    #            nothing
    # =========================================================================

    def update(self, playerPosition):
        speedX = speedY = self.speed

        if ( (self.isDamageTaken == True)  and  (self.damageTick == self.damageTickMax) ):
            self.isDamageTaken = False
            self.damageTick = 0

        # here we will restart a new rotation
        if (self.rotationCounter == 0):
            
            # set the maximum value that our angle of rotation
            # can reach
            self.rotationCounter = 45
            
            # this will determine the sign of our angle for use later when we
            # add to our angle of rotation
            sign = randint(1, 2)

            # if our sign is 1, we will be adding only positive values
            if (sign == 1):
                self.rotation = 1
            # otherwise, we will be adding only negative values
            else:
                self.rotation = -1

        # allow a maximum of only one full rotation
        if ( (self.rotationAngle > 360)  or  (self.rotationAngle < -360) ):
            self.rotationAngle = 0

        # increment our angle of rotation and decrement the available rotations
        # that we have left before creating a new one
        self.rotationAngle += self.rotation
        self.rotationCounter -= 1

        # always start with the original sprite image so we don't lose quality
        # from our rotations
        if (self.isDamageTaken == False):
            self.image = self.defaultImage.copy()
            self.image = pygame.transform.rotate(self.defaultImage, self.rotationAngle)
        else:
            self.image = self.hurtImage.copy()
            self.image = pygame.transform.rotate(self.hurtImage, self.rotationAngle)
            self.damageTick += 1


        # lets move the enemy towards the character
        x, y = playerPosition
        positionX = x - self.rect.centerx
        positionY = y - self.rect.centery
        hypotenuse = sqrt( (positionX * positionX) + (positionY * positionY) )

        # hack to make sure no division by zero exception is thrown
        if (hypotenuse == 0):
            hypotenuse = 0.1

        # logic in which the zombie may veer of course from hitting player
        if (self.rotationCounter != -1):
            if (randrange(0, 1) == 0):
                tempX = randrange(0, 2) == 0
                tempY = randrange(0, 2) == 0
                
                if (tempX == 0):
                    speedX *= -3
                elif (tempX == 1):
                    speedX *= 3
                else:
                    speedX = 0
                    
                if (tempY == 0):
                    speedY *= -1
                elif (tempY == 1):
                    speedY *= 2.5
                else:
                    speedY = 0                

        velocityX = (positionX / hypotenuse) * speedX
        velocityY = (positionY / hypotenuse) * speedY
        self.rect.centerx += velocityX
        self.rect.centery += velocityY
        
        if (self.rect.x < 0):
            self.rect.x = self.screenX - self.rect.width
        elif ( self.rect.x > (self.screenX - self.rect.width) ):
            self.rect.x = 0


    # === update ==============================================================
    # This method will reset the zombie's rect with the original rect.
    #
    #    input:
    #            nothing
    #
    #    output:
    #            nothing
    # =========================================================================

    def Reset(self):
        self.rect = self.defaultRect.copy()