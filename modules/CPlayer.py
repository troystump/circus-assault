#!/usr/bin/python
# =============================================================================
# File  : CPlayer.py (Spring 2011)
# Author: Troy Stump (troystump@csu.fullerton.edu)
# CWID  : 894788678
# Class : 386 Professor Shafae
# Assign: 06 (FINAL PROJECT)
# =============================================================================
# This class simulates a player that has a gun with bullets.
# =============================================================================

import pygame
from functions import RotateImage
from CGun import CGun
from CBullet import CBullet


class CPlayer(pygame.sprite.Sprite):

    # === __init__ ============================================================
    # This is the class initializer method.
    #
    #    input:
    #            screenX               --  width of display screen
    #            screenY               --  height of display screen
    #            player_image          --  image of the player
    #            player_hurt_image     --  image of the player getting hurt 
    #            player_rect           --  rect of the player image
    #            gun_image             --  image of the gun
    #            gun_rect              --  rect of the gun image
    #            gun_fire_image        --  image of the gun firing burst
    #            gun_fire_rect         --  rect of the gun firing burst image
    #            bullet_image          --  image of the bullet
    #            bullet_rect           --  rect of the bullet image
    #            sound_gun_fire        --  sound object of firing a gun
    #            sound_gun_outOfAmmo   --  sound object of gun out of ammo
    #            sound_gun_reload      --  sound object of gun reloading
    #
    #    output:
    #            nothing
    # =========================================================================

    def __init__(self, screenX, screenY, player_image, player_hurt_image, player_rect, gun_image, gun_rect, gun_fire_image, gun_fire_rect, bullet_image, bullet_rect, sound_gun_fire, sound_gun_outOfAmmo, sound_gun_reload):
        pygame.sprite.Sprite.__init__(self)
        self.defaultImage, self.defaultRect = player_image, player_rect.copy()
        self.defaultRect.center = (screenX / 2, screenY - 10 - (self.defaultRect.height / 2))
        
        self.image, self.rect = player_image, player_rect
        #self.rect.center = (screenX / 2, screenY - 10 - (self.rect.height / 2))
        self.rect = self.defaultRect.copy()

        self.rect.inflate_ip(-65, -65)

        self.gun = CGun(screenX, screenY, gun_image, gun_rect, gun_fire_image, gun_fire_rect)
        self.bulletsRemaining = self.maxBullets = 20
        
        self.bullets = [CBullet(screenX, screenY, bullet_image, bullet_rect) for i in xrange(self.maxBullets)]
        self.bulletGroup = pygame.sprite.RenderUpdates()
        self.bulletGroup.add(self.bullets)

        self.isFiring = self.isReloading = self.isOutOfAmmo = False

        self.hitPoints = self.maxHitPoints = 5
        self.score = 0
        self.ticks = 0
        self.currentWave = 0

        self.secondsAlive = 0

        self.sound_gun_fire = sound_gun_fire
        self.sound_gun_outOfAmmo = sound_gun_outOfAmmo
        self.sound_gun_reload = sound_gun_reload

        self.hurtImage = player_hurt_image

        self.isDamageTaken = False
        self.damageTick = 0
        self.damageTickMax = 4


    # === update ==============================================================
    # This method updates the status of the player, rotating the player's
    # image based on mouse position, as well as making sure all sounds do not
    # do not overlap and are executed appropriately with regard to the current
    # action that was requested to happen.
    #
    #    input:
    #            mousePosition   -- (x,y) coordinates of the mouse location
    #
    #    output:
    #            nothing
    # =========================================================================

    def update(self, mousePosition):
        key = pygame.key.get_pressed()
        if (key[pygame.K_LEFT]):
            self.defaultRect.x -= 3
            self.gun.rect.x -= 3
            if (self.defaultRect.x < 0):
                self.defaultRect.x = self.gun.rect.x = 0
        if (key[pygame.K_RIGHT]):
            self.defaultRect.x += 3
            self.gun.rect.x += 3
            if (self.defaultRect.x > (640 - self.defaultRect.width)):
                self.defaultRect.x = self.gun.rect.x = 640 - self.defaultRect.width
        if (key[pygame.K_DOWN]):
            self.defaultRect.y += 3
            self.gun.rect.y += 3
            if (self.defaultRect.y > 480 - self.defaultRect.height):
                self.defaultRect.y = self.gun.rect.y = 480 - self.defaultRect.height
        if (key[pygame.K_UP]):
            self.defaultRect.y -= 3
            self.gun.rect.y -= 3
            if (self.defaultRect.y < 0):
                self.defaultRect.y = self.gun.rect.y = 0
        
        
        # update player's angle to correspond with the current mouse position
        self.rotateImage = RotateImage(self.defaultImage, self.defaultRect, mousePosition)
        self.rotateHurtImage = RotateImage(self.hurtImage, self.defaultRect, mousePosition)
        #self.rotateImage = RotateImage(self.image, self.rect, mousePosition)
        #self.rotateHurtImage = RotateImage(self.hurtImage, self.rect, mousePosition)
        self.rotateRect = self.rotateImage.get_rect()
        #self.rotateRect.center = self.rect.center
        self.rotateRect.center = self.defaultRect.center
        self.rect = self.rotateRect.copy()
        self.rect.inflate_ip(-35, -35)

        if (self.isDamageTaken == True):
            if(self.damageTick == self.damageTickMax):
                self.isDamageTaken = False
            else:
                self.rotateImage = self.rotateHurtImage
                self.damageTick += 1

        # this will keep the player's gun in sync with the player's angle
        self.gun.update(mousePosition)
        self.bulletGroup.update(mousePosition)        
        
        # if we are reloading, don't allow any other actions until finished
        if ( (self.isReloading == True)  and  (pygame.time.get_ticks() - self.ticks) / 1000 >= self.sound_gun_reload.get_length() / 2 ):
            self.bulletsRemaining = self.maxBullets
            self.bulletGroup.add(self.bullets) 
            self.isReloading = False
            self.isOutOfAmmo = False
            self.ticks = 0

        # otherwise, check to see if we are out of ammo and let the player know
        # by playing an out of ammo sound effect
        elif ( (self.isOutOfAmmo == True)  and  (self.isReloading == False) ):
            self.sound_gun_outOfAmmo.stop()
            self.sound_gun_outOfAmmo.play()
        
        # otherwise we are going to fire a bullet
        elif (self.isFiring == True):
            self.sound_gun_fire.stop()
            self.sound_gun_fire.play()
            self.bulletsRemaining -= 1
            # send a bullet in motion to the directed path
            self.bullets[self.bulletsRemaining].isMoving = True
            self.bullets[self.bulletsRemaining].rect.center = self.defaultRect.center
            self.isFiring = False
            self.gun.isGunBurst = False

        self.isOutOfAmmo = False
        


    # === Draw ================================================================
    # This method draws the player and his/her gun and bullets to a surface.
    #
    #    input:
    #            surface   --  the surface to draw player, gun, and bullets on
    #
    #    output:
    #            nothing
    # =========================================================================

    def Draw(self, surface):
        surface.blit(self.rotateImage, self.rotateRect)
        self.gun.Draw(surface)
        self.bulletGroup.draw(surface)


    # === Fire ================================================================
    # This method draws the player and his/her gun and bullets to a surface.
    #
    #    input:
    #            surface   --  the surface to draw player, gun, and bullets on
    #
    #    output:
    #            returns true if we can and have a bullet to fire, otherwise
    #            false
    # =========================================================================

    def Fire(self):
        successful = True
        
        if (self.isReloading == True):
            successful = False
            
        if (self.OutOfAmmo() == True):
            successful = False

        if (successful == False):
            self.isFiring = False
            self.gun.isGunBurst = False
            return False

        self.isFiring = True
        self.gun.isGunBurst = True
        return True


    # === OutOfAmmo ===========================================================
    # This method simulates the player running out of ammunition.
    #
    #    input:
    #            surface   --  the surface to draw player, gun, and bullets on
    #
    #    output:
    #            returns true is we are out of ammo, otherwise false
    # =========================================================================

    def OutOfAmmo(self):
        if (self.bulletsRemaining != 0):
            self.isOutOfAmmo = False
            return False

        self.isOutOfAmmo = True        
        return True


    # === Reload ==============================================================
    # This method simulates reloading a gun.
    #
    #    input
    #            nothing
    #
    #    output
    #            returns true if we can reload, otherwise false
    # =========================================================================

    def Reload(self):
        if (self.ticks != 0):
            return True

        if (self.bulletsRemaining == self.maxBullets):
            self.isReloading = False
            return False

        self.isReloading = True
        self.ticks = pygame.time.get_ticks()
        self.sound_gun_reload.play()
        return True