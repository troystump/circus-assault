// ============================================================================
// File  : README.txt
// Author: Troy Stump (troystump@csu.fullerton.edu)
// CWID  : 894788678
// Class : 386 Professor Shafae
// Assign: 06 (FINAL PROJECT)
// ============================================================================
// NOTE:
//	-- This file describes the usage, features and bugs of my Assign 06
//		game, entitled "CIRCUS ASSAULT."
//
//
//
// CIRCUS ASSAULT SYNOPSIS
//
//
// PROJECT SCOPE:
//	-- This game has been designed using a 'top-down shooter' approach.
//		The game draws influence from a well known game on the internet
//		call "Zombie Assault." In this game, we utilize the idea of
//		rotating a stationary sprite to the direction of the current
//		mouse location. Further, the player can fire bullets from a 
//		gun, which also have a trajectory in the direction of the
//		current mouse location. With these two combined algorithms,
//		the game dynamics enable the player to shoot bullets in any
//		directon on the screen, with the player's sprite always
//		pointing in the same direction.
//	-- The enemies found in this game are zombies which utilize python's
//		built-in pseudo-random number generator. The zombies are able
//		to achieve 'random' sprite rotation, speed, and direction to
//		give off the effect of a zombie walking around mindlessly. On
//		the other hand, these enemies have a high probability of
//		staying on course in a direction to collide with the player's
//		sprite.  
//
//
// FEATURES:
//	-- STRATEGY:
//		1. The main strategy to this game is to use ammo wisely and
//			effectively hit multitple enemies with a single bullet.
//			This will help the player to advance in zombie waves,
//			which is essentially the goal of the game.
//	-- GRAPHICS:
//		1. 2D themed with a russian circus feel to it.
//	-- ANIMATION:
//		1. Player rotating based on mouse location
//		2. Bullets firing in any direction
//		3. Enemy Zombies moving about on the screen towards the player
//	-- MUSIC:
//		1. Streaming background music
//		2. Dark and war-like sounding at first, then happy and circus-
//			like sounding when fighting zombies  
//	-- SOUND EFFECTS:
//		3. Powerful sounds of a gun firing, reloading, etc.
//
//
// BUGS:
//	-- If the user is in fullscreen mode, reaches the game over screen,
//		and then returns to the main title screen, the user must push
//		the 'F' key twice to get the fullscreen toggle working again.
//		This is due to my current limitation in understanding how to
//		properly detect when the video mode is in fullscreen.		
//
//
// CREDITS:
//	-- Thanks to the many tutorials by Pete Shinners (creator of pygame)
//		http://www.pygame.org
//	-- Thanks to Andrew Sultan and Fidel Cabezas for helping me with a few
//		concerns I had in properly firing bullets accurately in any
//		direction deemed by the mouse location.
//	-- Thanks to David Rubenstein for his original compositions heard in the
//		background music throughout the game.
//		http://www.wonderful-music.com/music.html
//	-- Thanks to DeusX Sound Design for use of their royalty free sound
//		in all sound effects heard throughout the game.
//		http://www.deusx.com/freesoundeffects.html
//	-- Thanks to Matthew Harrower for use of his zombie image as part of a
//		collaboration seen in the main title screen background.
//		http://cghub.com/images/view/36765/
//	-- Player sprite was found with an unknown author.
//		http://img529.imageshack.us/img529/4931/zombieman.png
//	-- Zombie sprites were found with an unknown author.
//		http://imageshack.us/photo/my-images/20/vibrantspace.png/sr=1
//
//
// REQUIRED:
//	-- Python 2.6 interpreter
//	-- modules/config/config.py	(configuration for initial settings)
//	-- modules/config/__init__.py	(package directory file)
//	-- modules/CBullet.py		(class)
//	-- modules/CEnemy.py 		(class)
//	-- modules/CGame.py 		(class)
//	-- modules/CGun.py 		(class)
//	-- modules/CPlayer.py 		(class)
//	-- modules/__init__.py		(package directory file)
//	-- main.py			(entry to the program)
//	-- setup.py			(creates .exe with 'py2exe' module)
//
//
// EXTERNAL DEPENDENCIES:
//	-- MUSIC (2 files)
//	     data/audio/music/music_game_background.ogg
//	     data/audio/music/music_game_title.ogg
//	-- SOUNDS (7 files)
//	     data/audio/sounds/sound_game_gameOver.ogg
//	     data/audio/sounds/sound_gun_fire.ogg
//	     data/audio/sounds/sound_gun_outOfAmmo.ogg
//	     data/audio/sounds/sound_gun_reload.ogg
//	     data/audio/sounds/sound_level_waveComplete.ogg
//	     data/audio/sounds/sound_player_killEnemy.ogg
//	     data/audio/sounds/sound_player_takeDamage.ogg
//	-- GRAPHICS (18 files)
//	     data/graphics/graphic_bullet_bullet.png
//	     data/graphics/graphic_enemy_zombie_01.png
//	     data/graphics/graphic_enemy_zombie_02.png
//	     data/graphics/graphic_enemy_zombie_03.png
//	     data/graphics/graphic_enemy_zombie_takeDamage.png
//	     data/graphics/graphic_game_background.png
//	     data/graphics/graphic_game_gameover.png
//	     data/graphics/graphic_game_title.png
//	     data/graphics/graphic_game_titleScreenBg.png
//	     data/graphics/graphic_game_titleScreenPlayGame.png
//	     data/graphics/graphic_game_titleScreenPlayGameMouseOver.png
//	     data/graphics/graphic_game_titleScreenPlayGameMouseOverArrows.png
//	     data/graphics/graphic_game_tutorial.png
//	     data/graphics/graphic_gun_fire.png
//	     data/graphics/graphic_gun_gun.png
//	     data/graphics/graphic_icon_exe.png
//	     data/graphics/graphic_player_player.png
//	     data/graphics/graphic_player_player_takeDamage.png
//	-- FONTS (1 file)
//	     data/fonts/font_hobo.otf
//	-- ICONS (1 file)
//	     data/icons/icon_exe.ico
//
//
// INSTRUCTIONS FOR BUILDING:
//	-- Please make sure all files are in their respective relative paths,
//	 	given by the 'REQUIRED' and 'EXTERNAL DEPENDENCIES' catagories
//		above. Finally, run the 'main.py' to start the game. 
// ============================================================================