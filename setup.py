#!/usr/bin/python
# =============================================================================
# File  : setup.py (Spring 2011)
# Author: Troy Stump (troystump@csu.fullerton.edu)
# CWID  : 894788678
# Class : 386 Professor Shafae
# Assign: 06 (FINAL PROJECT)
# =============================================================================
# This program creates a stand-alone executable for the game: CIRCUS ASSAULT
# =============================================================================

from distutils.core import setup
import py2exe
import sys
import os
import glob, shutil
import pygame


origIsSystemDLL = py2exe.build_exe.isSystemDLL
def isSystemDLL(pathname):
       if os.path.basename(pathname).lower() in ["sdl_ttf.dll", "libogg-0.dll"]:
               return 0
       return origIsSystemDLL(pathname)
py2exe.build_exe.isSystemDLL = isSystemDLL

setup(name='CIRCUS ASSAULT',
      version='1.0',
      description='Top-Down Shooter',
      author='Troy Stump',
      author_email='troystump@csu.fullerton.edu',
      windows = [{
                'script': 'main.py',
                'icon_resources': [(1, 'data/icons/icon_exe.ico')],
                'copyright': 'Troy Stump',
                'dest_base': 'Circus Assault',
                }],      
      options = {"py2exe": {
            "optimize": 2,
            "compressed": 0,
            "bundle_files": 1,
                }},
        zipfile = None,
      data_files = [
    ("data//audio//music", glob.glob("data//audio//music//*.ogg")),
    ("data//audio//sounds", glob.glob("data//audio//sounds//*.ogg")),
    ("data//graphics", glob.glob("data//graphics//*.png")),
    ("data//fonts", glob.glob("data//fonts//*.otf")),
    ("data//icons", glob.glob("data//icons//*.ico"))])