# =============================================================================
# File  : main.py (Spring 2011)
# Author: Troy Stump (troystump@csu.fullerton.edu)
# CWID  : 894788678
# Class : 386 Professor Shafae
# Assign: 06 (FINAL PROJECT)
# =============================================================================
# This program is the driver for the game CIRCUS ASSAULT.
# =============================================================================

from modules.CGame import CGame


# === main ====================================================================
# This is the main method which will power the game
# =============================================================================

def main():
    game = CGame()
    game.__init__()
    game.Run()
    
if __name__ == '__main__':
    main()